#include <bj/cards/Card.hpp>

#include <algorithm>
#include <ostream>

namespace bj::cards {

    std::uint8_t Card::value() const noexcept {
        // Ace 11 unless would go over
        if (is_ace()) {
            return 11;
        }
        // +2 because the underlying value of ranks starts from 0
        // min 10 because face cards are all valued at 10
        return std::min(static_cast<std::uint8_t>(rank) + 2, 10);
    }

    bool Card::is_ace() const noexcept {
        return Rank::Ace == rank;
    }


    std::ostream& operator<<(std::ostream& lhs, const Card& rhs) {
        lhs << rhs.rank << " of " << rhs.suit;
        return lhs;
    }

} // bj::cards

#include <bj/cards/Rank.hpp>

#include <ostream>

namespace bj::cards {

    std::ostream& operator<<(std::ostream& lhs, const Rank rhs) {
        switch (rhs) {
            case Rank::Two:
                return lhs << "Two";
            case Rank::Three:
                return lhs << "Three";
            case Rank::Four:
                return lhs << "Four";
            case Rank::Five:
                return lhs << "Five";
            case Rank::Six:
                return lhs << "Six";
            case Rank::Seven:
                return lhs << "Seven";
            case Rank::Eight:
                return lhs << "Eight";
            case Rank::Nine:
                return lhs << "Nine";
            case Rank::Ten:
                return lhs << "Ten";
            case Rank::Jack:
                return lhs << "Jack";
            case Rank::Queen:
                return lhs << "Queen";
            case Rank::King:
                return lhs << "King";
            case Rank::Ace:
                return lhs << "Ace";
            case Rank::COUNT:
                // TODO...
                break;
        }

        // TODO: invalid enum

        return lhs;
    }

} // bj::cards

#include <bj/cards/Suit.hpp>

#include <ostream>

namespace bj::cards {

    std::ostream& operator<<(std::ostream& lhs, const Suit rhs) {
        switch (rhs) {
            case Suit::Hearts:
                return lhs << "Hearts";
            case Suit::Diamonds:
                return lhs << "Diamonds";
            case Suit::Clubs:
                return lhs << "Clubs";
            case Suit::Spades:
                return lhs << "Spades";
            case Suit::COUNT:
                // TODO...
                break;
        }

        // TODO: invalid enum

        return lhs;
    }

} // bj::cards

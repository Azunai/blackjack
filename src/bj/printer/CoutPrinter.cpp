#include <bj/printer/CoutPrinter.hpp>

#include <iostream>

namespace bj::printer {

    void CoutPrinter::print(const std::string& msg) {
        std::cout << msg << '\n';
    }

    void CoutPrinter::print_error(const std::string& msg) {
        std::cerr << msg << '\n';
    }

} // bj::printer

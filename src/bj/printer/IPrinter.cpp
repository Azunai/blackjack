#include <bj/printer/IPrinter.hpp>

#include <stdexcept>
#include <utility>

namespace bj::printer {

    namespace {
        std::unique_ptr<IPrinter> s_printer;
    } // namespace

    void set_instance(std::unique_ptr<IPrinter>&& p) {
        s_printer = std::move(p);
    }

    IPrinter& instance() {
        if (!s_printer) {
            throw std::runtime_error("No printer set");
        }
        return *s_printer;
    }

    void print(const std::string& msg) {
        instance().print(msg);
    }

    void print_error(const std::string& msg) {
        instance().print_error(msg);
    }

} // bj::printer

#include <bj/players/Dealer.hpp>

#include <bj/game/Deck.hpp>
#include <bj/game/Game.hpp>
#include <bj/printer/IPrinter.hpp>

namespace bj::players {

    void Dealer::init(game::Deck& deck) {
        m_hand.add_card(deck.take());
    }

    bool Dealer::act(game::Game& game) {
        const auto maxPlayerHand = game.highest_valid_player_hand();
        while (m_hand.is_valid() && m_hand.value() < maxPlayerHand) {
            printer::print("************************");
            game.print_hands();
            printer::print("Dealer hits!");
            m_hand.add_card(game.deck().take());
        }
        game.print_hands();
        if (m_hand.is_valid()) {
            printer::print("Dealer stands!");
        }
        return false;
    }

    const game::Hand& Dealer::hand() const noexcept {
        return m_hand;
    }

} // bj::players

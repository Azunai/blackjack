#include <bj/players/Human.hpp>

#include <bj/game/Deck.hpp>
#include <bj/game/Game.hpp>
#include <bj/game/Utils.hpp>
#include <bj/printer/IPrinter.hpp>

#include <iomanip>
#include <iostream>

namespace bj::players {

    namespace {

        enum class Action {
            Invalid,
            Hit,
            Stand,
            Quit,
        };

        [[nodiscard]] Action parse_input(const std::string& input) noexcept {
            if (input.empty()) {
                return Action::Invalid;
            }
            switch (input[0]) {
                case 'h':
                    return Action::Hit;
                case 's':
                    return Action::Stand;
                case 'q':
                    return Action::Quit;
                default:
                    return Action::Invalid;
            }
        }

    } // namespace

    void Human::init(game::Deck& deck) {
        m_hand.add_card(deck.take());
        m_hand.add_card(deck.take());
    }

    bool Human::act(game::Game& game) {
        bool quit {false};
        bool loop {true};
        Action action {Action::Invalid};
        std::string input;
        do {
            printer::print("************************");
            game.print_hands();
            printer::print("Select action: h = hit, s = stand, q = quit");

            std::getline(std::cin, input);
            action = parse_input(input);

            switch (action) {
                case Action::Invalid:
                    printer::print(game::make_string("Invalid input: ", std::quoted(input)));
                    break;
                case Action::Hit:
                    printer::print("Player hits!");
                    m_hand.add_card(game.deck().take());
                    loop = m_hand.is_valid();
                    break;
                case Action::Stand:
                    printer::print("Player stands!");
                    loop = false;
                    break;
                case Action::Quit:
                    printer::print("Player quits!");
                    loop = false;
                    quit = true;
                    break;
            }

        } while (loop);

        return quit;
    }

    const game::Hand& Human::hand() const noexcept {
        return m_hand;
    }

} // bj::players

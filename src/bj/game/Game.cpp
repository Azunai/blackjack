#include <bj/game/Game.hpp>

#include <bj/game/Utils.hpp>
#include <bj/players/Dealer.hpp>
#include <bj/players/Human.hpp>
#include <bj/printer/IPrinter.hpp>

#include <algorithm>
#include <stdexcept>

namespace bj::game {

    namespace {

        template <typename T, typename D>
        void check_ptr(const std::unique_ptr<T, D>& p, const char* const who) {
            // TODO: better error handling
            if (!p) {
                throw std::runtime_error(game::make_string(
                    "Tried to get ", who, ", but there was none"
                ));
            }
        }

    } // namespace

    Game::Game() :
        m_dealer(std::make_unique<players::Dealer>()),
        m_player(std::make_unique<players::Human>())
    {
        m_dealer->init(m_deck);
        m_player->init(m_deck);
    }

    void Game::play() {

        if (m_player->act(*this)) {
            // Player wanted to quit
            return;
        }
        m_dealer->act(*this);

        const auto& handPlayer { m_player->hand() };
        const auto& handDealer { m_dealer->hand() };
        const bool playerValid {handPlayer.is_valid()};
        const bool dealerValid {handDealer.is_valid()};
        const auto valuePlayer {handPlayer.value()};
        const auto valueDealer {handDealer.value()};

        // const auto 

        if (!playerValid && !dealerValid) {
            printer::print_error("Both player and dealer busted! That shouldn't be possible!?");
            return;
        }
        if (!playerValid) {
            printer::print("Player busts!");
            printer::print("Dealer wins!");
        } else if (!dealerValid) {
            printer::print("Dealer busts!");
            printer::print("Player wins!");
        } else if (valuePlayer == valueDealer) {
            printer::print(make_string("Tie at ", std::uint32_t{valuePlayer}));
        } else if (valuePlayer < valueDealer) {
            printer::print("Dealer wins!");
        } else /* valuePlayer > valueDealer */ {
            printer::print("Player wins!");
        }

    }

    std::uint8_t Game::highest_valid_player_hand() const {
        if (!m_player) {
            return 0;
        }
        const auto& hand { m_player->hand() };
        return hand.is_valid() ? hand.value() : 0;
    }

    Deck& Game::deck() noexcept {
        return m_deck;
    }

    const players::IPlayer& Game::dealer() const {
        check_ptr(m_dealer, "Dealer");
        return *m_dealer;
    }
    const players::IPlayer& Game::player() const {
        check_ptr(m_player, "Player");
        return *m_player;
    }

    void Game::print_hands() {
        printer::print(game::make_string("Player hand: ", player().hand()));
        printer::print(game::make_string("Dealer hand: ", dealer().hand()));
    }

} // bj::game


#include <bj/game/Deck.hpp>

#include <algorithm>
#include <random>

namespace bj::game {

    Deck::Deck() {
        // TODO: Better seeding
        std::mt19937 engine;
        engine.seed(std::random_device()());

        std::shuffle(m_deck.begin(), m_deck.end(), engine);
    }

    cards::Card Deck::take() {
        // TODO: Better error handling
        return m_deck.at(m_index++);
    }

} // bj::game

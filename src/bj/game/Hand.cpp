#include <bj/game/Hand.hpp>

#include <ostream>

namespace bj::game {

    namespace {
        constexpr std::uint8_t Limit {21};
    } // namespace

    std::uint8_t Hand::add_card_value(const cards::Card& card, const std::uint8_t currentValue) {
        const auto cardValue { card.value() };
        return currentValue + (
            (card.is_ace() && ((currentValue + cardValue) > Limit)) ?
            std::uint8_t{1} : cardValue
        );
    }

    void Hand::add_card(const cards::Card& card) {
        m_value = add_card_value(card, m_value);
        m_cards.push_back(card);
    }

    std::uint8_t Hand::value() const noexcept {
        return m_value;
    }

    bool Hand::is_valid() const noexcept {
        return this->value() <= Limit;
    }

    std::ostream& operator<<(std::ostream& lhs, const Hand& rhs) {
        for (const auto& c : rhs.m_cards) {
            lhs << c << ", ";
        }
        // Move write position two steps back to overwrite the last ", "
        lhs.seekp(-2, std::ios_base::cur);
        lhs << " (total value: " << std::uint32_t{rhs.value()} << ')';
        return lhs;
    }

} // bj::game

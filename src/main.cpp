
#include <bj/game/Game.hpp>
#include <bj/printer/CoutPrinter.hpp>

int main() {

    bj::printer::set_instance(
        std::make_unique<bj::printer::CoutPrinter>()
    );
    bj::game::Game game;
    game.play();

    return 0;
}

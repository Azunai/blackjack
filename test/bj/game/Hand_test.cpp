#include <bj/game/Hand.hpp>

#include <gtest/gtest.h>

#include <tuple>

using namespace bj::cards;

// Input card, input current value, expected value out
using ParamTuple = std::tuple<Card, std::uint8_t, std::uint8_t>;

struct HandTest : public testing::TestWithParam<ParamTuple> {};

TEST_P(HandTest, testAddCardValue) {
    const auto& [card, oldValue, expectedValue] {GetParam()};
    EXPECT_EQ(bj::game::Hand::add_card_value(card, oldValue), expectedValue);
}

// Queue Motörhead
INSTANTIATE_TEST_SUITE_P(ace, HandTest, testing::ValuesIn({
    ParamTuple{Card{Rank::Ace, Suit::Spades}, 21, 22},
    ParamTuple{Card{Rank::Ace, Suit::Spades}, 20, 21},
    ParamTuple{Card{Rank::Ace, Suit::Spades}, 19, 20},
    ParamTuple{Card{Rank::Ace, Suit::Spades}, 18, 19},
    ParamTuple{Card{Rank::Ace, Suit::Spades}, 17, 18},
    ParamTuple{Card{Rank::Ace, Suit::Spades}, 16, 17},
    ParamTuple{Card{Rank::Ace, Suit::Spades}, 15, 16},
    ParamTuple{Card{Rank::Ace, Suit::Spades}, 14, 15},
    ParamTuple{Card{Rank::Ace, Suit::Spades}, 13, 14},
    ParamTuple{Card{Rank::Ace, Suit::Spades}, 12, 13},
    ParamTuple{Card{Rank::Ace, Suit::Spades}, 11, 12},
    ParamTuple{Card{Rank::Ace, Suit::Spades}, 10, 21},
    ParamTuple{Card{Rank::Ace, Suit::Spades}, 9, 20},
}));

INSTANTIATE_TEST_SUITE_P(notAce, HandTest, testing::ValuesIn({
    ParamTuple{Card{Rank::King, Suit::Spades}, 20, 30},
    ParamTuple{Card{Rank::Queen, Suit::Spades}, 20, 30},
    ParamTuple{Card{Rank::Jack, Suit::Spades}, 20, 30},
    ParamTuple{Card{Rank::Ten, Suit::Spades}, 20, 30},
    ParamTuple{Card{Rank::Nine, Suit::Spades}, 20, 29},
    ParamTuple{Card{Rank::Eight, Suit::Spades}, 20, 28},
    ParamTuple{Card{Rank::Seven, Suit::Spades}, 20, 27},
    ParamTuple{Card{Rank::Six, Suit::Spades}, 20, 26},
    ParamTuple{Card{Rank::Five, Suit::Spades}, 20, 25},
    ParamTuple{Card{Rank::Four, Suit::Spades}, 20, 24},
    ParamTuple{Card{Rank::Three, Suit::Spades}, 20, 23},
    ParamTuple{Card{Rank::Two, Suit::Spades}, 20, 22},
}));

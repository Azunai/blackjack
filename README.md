### Usage:
#### Prerequisites:
- C++ 17 compiler
- make

#### Optional prerequisites:
- ctest
- gtest


```
mkdir blackjack && cd blackjack
git clone https://gitlab.com/Azunai/blackjack.git .
mkdir build && cd build
cmake ../
make
ctest
./blackjack
```

#### Without tests:
If you do not have ctest/gtest, substitute
```
cmake -DBJ_ENABLE_TESTING=FALSE ../
```
and skip
```
ctest
```

#pragma once

#include <cstdint>
#include <iosfwd>

namespace bj::cards {

    enum class Rank : std::uint8_t {
        Two,    ///< 2
        Three,  ///< 3
        Four,   ///< 4
        Five,   ///< 5
        Six,    ///< 6
        Seven,  ///< 7
        Eight,  ///< 8
        Nine,   ///< 9
        Ten,    ///< 10
        Jack,   ///< J
        Queen,  ///< Q
        King,   ///< K
        Ace,    ///< A

        /**
         * @warning For array generation only, not a valid enumeration value.
         * TODO: C++29? Remove and use reflection instead.
         */
        COUNT
    };

    std::ostream& operator<<(std::ostream& lhs, Rank rhs);

} // bj::cards

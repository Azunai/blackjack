#pragma once

#include <bj/cards/Card.hpp>

#include <array>

namespace bj::cards {

    [[nodiscard]] constexpr auto makeDeck() {
        constexpr auto suitCount { static_cast<std::size_t>(Suit::COUNT) };
        constexpr auto rankCount { static_cast<std::size_t>(Rank::COUNT) };
        std::array<Card, suitCount * rankCount> deck;

        for (std::size_t s = 0; s < suitCount; ++s) {
            for (std::size_t r = 0; r < rankCount; ++r) {
                deck[(s * rankCount) + r] = Card{static_cast<Rank>(r), static_cast<Suit>(s)};
            }
        }
        return deck;
    }

} // bj::cards

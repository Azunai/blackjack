#pragma once

#include <bj/cards/Rank.hpp>
#include <bj/cards/Suit.hpp>

namespace bj::cards {

    struct Card {

        constexpr Card() noexcept = default;
        constexpr Card(const Rank rank_, const Suit suit_) noexcept :
            rank(rank_),
            suit(suit_)
        {}

        Rank rank {Rank::Ace};
        Suit suit {Suit::Hearts};

        [[nodiscard]] std::uint8_t value() const noexcept;
        [[nodiscard]] bool is_ace() const noexcept;

    }; // Card

    std::ostream& operator<<(std::ostream& lhs, const Card& rhs);

} // bj::cards

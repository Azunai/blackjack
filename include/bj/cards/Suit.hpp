#pragma once

#include <cstdint>
#include <iosfwd>

namespace bj::cards {

    enum class Suit : std::uint8_t {
        Hearts,     ///< ♥
        Diamonds,   ///< ♦
        Clubs,      ///< ♣
        Spades,     ///< ♠

        /**
         * @warning For array generation only, not a valid enumeration value.
         * TODO: C++29? Remove and use reflection instead.
         */
        COUNT
    };

    std::ostream& operator<<(std::ostream& lhs, Suit rhs);

} // bj::cards

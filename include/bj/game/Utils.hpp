#pragma once

#include <sstream>
#include <utility>

namespace bj::game {

    /**
     * @param args Any number of args which support std::ostream::operator<<
     * @return String containing the results of the above call for each arg.
     */
    template <typename ... Args>
    [[nodiscard]] std::string make_string(Args&& ... args) {
        std::ostringstream str;
        ((str << std::forward<Args>(args)), ...);
        return str.str();
    }

} // bj::game

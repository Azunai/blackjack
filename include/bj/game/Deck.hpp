#pragma once

#include <bj/cards/Utils.hpp>

namespace bj::game {

    struct Deck {

        Deck();

        /**
         * Take a card from the deck.
         */
        [[nodiscard]] cards::Card take();

    private:
        using DeckType = decltype(cards::makeDeck());

        DeckType m_deck {cards::makeDeck()};
        std::uint8_t m_index {0};

    }; // Deck

} // bj::game

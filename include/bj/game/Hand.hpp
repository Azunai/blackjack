#pragma once

#include <bj/cards/Card.hpp>

#include <vector>

namespace bj::game {

    struct Hand {

        /**
         * Add a card to this hand.
         * @param card Card to add.
         */
        void add_card(const cards::Card& card);

        /**
         * Retrieve the total value of the hand.
         */
        [[nodiscard]] std::uint8_t value() const noexcept;

        [[nodiscard]] bool is_valid() const noexcept;

        friend std::ostream& operator<<(std::ostream& lhs, const Hand& rhs);

        /**
         * Adds the card value to the current value. This handles the spacial case of Ace = 1.
         * @param card New card to add.
         * @param currentValue Current total value.
         * @return New total value.
         */
        [[nodiscard]] static std::uint8_t add_card_value(const cards::Card& card, std::uint8_t currentValue);

    private:

        /**
         * TODO: Use an array of optional<Card>, as the max number of cards a hand can have is known:
         *      4 * 1 (ace when it would go over 21)
         *      + 4 * 2
         *      + ...
         *      <= 21
         */
        /**
         * TODO: Cards aren't really necessary here,
         *  could store a single string / ostringstream containing the representations.
         */
        std::vector<cards::Card> m_cards;

        /**
         * Total value of the hand. > 21 goes bust
         */
        std::uint8_t m_value {0};

    };

} // bj::game

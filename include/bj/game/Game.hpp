#pragma once

#include <bj/players/IPlayer.hpp>
#include <bj/game/Deck.hpp>

#include <memory>

namespace bj::game {

    struct Game {

        Game();

        void play();

        [[nodiscard]] std::uint8_t highest_valid_player_hand() const;
        [[nodiscard]] Deck& deck() noexcept;
        [[nodiscard]] const players::IPlayer& dealer() const;
        [[nodiscard]] const players::IPlayer& player() const;

        void print_hands();

    private:
        std::unique_ptr<players::IPlayer> m_dealer;
        std::unique_ptr<players::IPlayer> m_player; // TODO: multiple players: vector
        Deck m_deck;

    }; // Game

} // bj::game

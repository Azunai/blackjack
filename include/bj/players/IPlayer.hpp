#pragma once

namespace bj::game {

    struct Deck;
    struct Game;
    struct Hand;

} // bj::game

namespace bj::players {

    struct IPlayer {

        IPlayer() noexcept = default;
        IPlayer(IPlayer&&) noexcept = default;
        IPlayer(const IPlayer&) noexcept = default;
        IPlayer& operator=(IPlayer&&) noexcept = default;
        IPlayer& operator=(const IPlayer&) noexcept = default;
        virtual ~IPlayer() = default;

        /**
         * Override to do initialization.
         * RAII would be 'better', but you can't forget to override pure virtuals.
         * (At least with a depth of 1, with deeper inheritance things would change...)
         */
        virtual void init(game::Deck& deck) = 0;

        /**
         * @param game Current game state.
         * @return True if wants to quit the game.
         */
        virtual bool act(game::Game& game) = 0;

        /**
         * Each player should have a hand.
         * No inheritance from IHand, because Player is not a hand, player has a hand.
         */
        virtual const game::Hand& hand() const noexcept = 0;

    };

} // bj::players

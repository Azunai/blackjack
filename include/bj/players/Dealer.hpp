#pragma once

#include <bj/players/IPlayer.hpp>
#include <bj/game/Hand.hpp>

namespace bj::players {

    struct Dealer : public IPlayer {

        void init(game::Deck& deck) override;
        bool act(game::Game& game) override;

        const game::Hand& hand() const noexcept override;

    private:

        game::Hand m_hand;

    }; // Dealer

} // bj::players

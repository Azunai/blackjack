#pragma once

#include <memory>
#include <string>

namespace bj::printer {

    struct IPrinter {

        IPrinter() noexcept = default;
        IPrinter(IPrinter&&) noexcept = default;
        IPrinter(const IPrinter&) noexcept = default;
        IPrinter& operator=(IPrinter&&) noexcept = default;
        IPrinter& operator=(const IPrinter&) noexcept = default;
        virtual ~IPrinter() = default;

        virtual void print(const std::string& msg) = 0;
        virtual void print_error(const std::string& msg) = 0;
    };

    void set_instance(std::unique_ptr<IPrinter>&& p);
    IPrinter& instance();

    void print(const std::string& msg);
    void print_error(const std::string& msg);

} // bj::printer

#pragma once

#include <bj/printer/IPrinter.hpp>

namespace bj::printer {

    struct CoutPrinter : public IPrinter {
        void print(const std::string& msg) override;
        void print_error(const std::string& msg) override;
    };

} // bj::printer
